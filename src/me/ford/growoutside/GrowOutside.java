package me.ford.growoutside;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class GrowOutside extends JavaPlugin implements Listener {
	private List<Material> exceptions = new ArrayList<Material>();
	private List<Material> allowAbove = new ArrayList<Material>();
	private final Logger LOGGER = Logger.getLogger("GrowOutside");
	
	
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		allowAbove.add(Material.AIR);
		loadConfiguration();
	}
	
	private void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		if (getConfig().contains("exclude")) {
			List<String> excludeStrings = getConfig().getStringList("exclude");
			for (String toExclude : excludeStrings) {
				Material matExclude = Material.matchMaterial(toExclude);
				if (matExclude == null) {
					LOGGER.info("Could not find material to exclude: " + toExclude);
				} else {
					exceptions.add(matExclude);
				}
			}
			if (!excludeStrings.isEmpty()) {
				LOGGER.info("Excecptions:" + exceptions);
			}
		}
		if (getConfig().contains("allow-above")) {
			List<String> allowStrings = getConfig().getStringList("allow-above");
			for (String toAllow : allowStrings) {
				Material matAllow = Material.matchMaterial(toAllow);
				if (matAllow == null) {
					LOGGER.info("Could not find material to allow: " + toAllow);
				} else {
					allowAbove.add(matAllow);
				}
			}
			if (!allowStrings.isEmpty()){
				LOGGER.info("Allowed:" + allowAbove);
			}
		}
		saveConfig();
	}
	
	private boolean checkIfAllowed(Block grower) {
		if (!exceptions.contains(grower.getType())){
			for (int y = 1; y < 256 - grower.getY(); y++) {
				Block above = grower.getRelative(0, y, 0);
				if (!allowAbove.contains(above.getType()))
					return false;
			}
		}
		return true;
	}
	
	@EventHandler
	public void onBlockGrowEvent(BlockGrowEvent event) {
		Block grower = event.getBlock();
		if (!checkIfAllowed(grower)) {
			event.setCancelled(true);
		}
	}
	
	private boolean commandPreProcess(CommandSender sender) {
		// if not a player, cannot use
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "A player needs to use this command!");
			return false;
		}
		Player player = (Player) sender;
		Block target = player.getTargetBlock(null, 5);
		// if no target, cannot use
		if (target == null) {
			sender.sendMessage(ChatColor.RED + "Need to point at a block!");
			return false;
		}
		return true;
	}
	
	private boolean command(CommandSender sender, Commands cmd) {
		if (!commandPreProcess(sender)) {
			return true;
		}
		Player player = (Player) sender;
		Block target = player.getTargetBlock(null, 5);
		// get appropriate list
		List<Material> list = (cmd.get() == AllowExclude.EXCLUDE)?exceptions:allowAbove;
		if (cmd.type() == AddDel.DEL) { 
			// if removing, check if in list
			if (!list.contains(target.getType())) {
				sender.sendMessage(ChatColor.RED + "Block not in list!");
				return true;
			}
			// if in list, remove and message
			list.remove(target.getType());
			sender.sendMessage(ChatColor.BLUE + "Succesfully removed the block from the list: " +
					ChatColor.AQUA + target.getType().name());
		} else {
			// if adding, check if already in list
			if(list.contains(target.getType())) {
				sender.sendMessage(ChatColor.RED + "Block already on list!");
				return true;
			}
			// if not in list, add and message
			list.add(target.getType());
			sender.sendMessage(ChatColor.BLUE + "Succesfully added the block to the list: " +
					ChatColor.AQUA + target.getType().name());
		}
		// make a lit of material names
		List<String> stringList = list.stream().map(Material::name).collect(Collectors.toList());
		// get the path to save to
		String savePath = cmd.get() == AllowExclude.EXCLUDE ? "exclude" : "allow-above";
		getConfig().set(savePath, stringList); // save to config
		saveConfig(); // save the file
		return true;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("growoutsideallow")) {
			return command(sender, Commands.ADD_ALLOW);
		} else if (command.getName().equalsIgnoreCase("growoutsideexclude")) {
			return command(sender, Commands.ADD_EXCLUDE);
		} else if (command.getName().equalsIgnoreCase("growoutsidedisallow")) {
			return command(sender, Commands.DEL_ALLOW);
		} else if (command.getName().equalsIgnoreCase("growoutsidedelexclude")) {
			return command(sender, Commands.DEL_EXCLUDE);
		}
		return false;
	}
	
	private enum AddDel {
		ADD,
		DEL;
	}
	
	private enum AllowExclude {
		ALLOW,
		EXCLUDE;
	}
	
	private enum Commands {
		ADD_ALLOW,
		ADD_EXCLUDE,
		DEL_ALLOW,
		DEL_EXCLUDE;
		
		public AddDel type(){
			if (name().startsWith("ADD")) {
				return AddDel.ADD;
			} else {
				return AddDel.DEL;
			}
		}
		
		public AllowExclude get(){
			if (name().endsWith("ALLOW")) {
				return AllowExclude.ALLOW;
			} else {
				return AllowExclude.EXCLUDE;
			}
		}
	}

}
